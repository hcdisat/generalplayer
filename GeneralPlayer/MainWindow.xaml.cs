﻿
using GeneralPlayer.ViewModels;
//using GeneralPlayer.Models;
using MahApps.Metro.Controls;
using System.Windows.Media;

namespace GeneralPlayer
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            #region Models
            //MediaFileRepository mediaRepo = new MediaFileRepository();
            //PluginRepository pluginRepo = new PluginRepository();
            //CollectionRepository collRepo = new CollectionRepository();
            //var media = mediaRepo.GetById(1);
            //var plugin = pluginRepo.GetAll().ToList();
            //var collection = collRepo.GetById(1);            
            //#endregion

            //#region ViewModels
            //NextInQueueVM queueDisplay = new NextInQueueVM
            //{
            //    Model = media
            //};

            //LibraryVM libVm = new LibraryVM
            //{
            //    Model = plugin
            //};

            //_player.DataContext = new PlayerVM
            //{
            //    CurrentFile = new Uri(media.Path)
            //};

            //_mediaDetails.DataContext = new MediaDetailVM
            //{
            //    Model = collection
            //};

            //#endregion

            //#region Set ViewModel to Views
            //_queueDisplay.DataContext = queueDisplay;
            //_library.DataContext = libVm;
            #endregion

            var brush = (_player.Resources["Media"] as VisualBrush);
            this.DataContext =  new MainVM(this, brush);
        }
    }
}