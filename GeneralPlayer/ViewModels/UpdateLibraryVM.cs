﻿using GeneralPlayer.Configuration;
using GeneralPlayer.Models;
using GeneralPlayer.Models.Adapters;
using GeneralPlayer.Models.Repositories;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralPlayer.ViewModels.Extensions;
using System.Windows.Input;
using GeneralPlayer.Core;
using MahApps.Metro.Controls;

using System.Configuration;
using System.Data.SQLite;
using System.Collections.ObjectModel;
namespace GeneralPlayer.ViewModels
{
    public class UpdateLibraryVM : Core.ViewModelBase
    {

        public LibraryVM Library { get; set; }

        private ICommand _updateDatabaseCommand;

        public ICommand UpdateDatabaseCommand
        {
            get
            {
                return _updateDatabaseCommand ??
                    (_updateDatabaseCommand = new RelayCommand(() =>
                    {
                        Action getResult = async () =>
                        {
                            string title = "Alerta",
                            message = "¿Seguro desea actualizr la libreria en este momento?, Esto puede tardar mucho.";
                            MessageDialogStyle style = MessageDialogStyle.AffirmativeAndNegative;

                            var dialogResult = await getMetroWindow()
                                .ShowMessageAsync(title, message, style, this.MetroDialogSettings());

                            if (dialogResult == MessageDialogResult.Affirmative)
                                updateLibrary();
                        };
                        getResult();

                    }));
            }
        }


        /// <summary>
        /// Updates The Library
        /// </summary>
        private async void updateLibrary()
        {
            await Task.Run(() =>
            {
                string databaseName = ConfigurationManager.AppSettings["DB_NAME"];
                List<Plugin> plugins = null;
                List<ICollection<Container>> containers = null;
                string target = string.Empty;


                //Copy the Database                
                if (File.Exists(databaseName))
                {
                    target = ConfigurationFolders.BuildDirectoryPath(databaseName,
                            ConfigurationFolders.Folders.DatabaseFolder);

                    if (File.Exists(target))
                    {
                        // If theres plugins or containers in the actual database get 
                        // a copy of those
                        plugins = RepositoryFactory
                            .Create<PluginRepository>()
                            .GetAll().ToList();

                        containers = (from p in plugins
                                      select p.Containers).ToList();

                        //Truncate the data of the media tables
                        RepositoryFactory.Create<MediaFileRepository>().Truncate();
                        RepositoryFactory.Create<CollectionRepository>().Truncate();                      
                    }


                    // Recreates All the folder
                    ConfigurationFolders.FolderMaker.CreateFolders(true);
                }


                //Build Content                           
                if (containers != null)
                {
                    foreach (var item in containers)
                    {
                        foreach (Container container in item)
                        {
                            LocalFilesGateway gateway = new LocalFilesGateway(container.Path);
                            var content = gateway.GetContent() as List<System.IO.DirectoryInfo>;                           
                            var fileAdapter = new LocalFIleAdapter();

                            // Various folders found
                            if (content.Count > 0)
                            {
                                foreach (var folder in content)
                                {
                                    //Saving Collections
                                    Collection collection = fileAdapter.SaveDirectory(folder, container.Id);

                                    //Saving Files
                                    fileAdapter.SaveFilesInDirectory(folder, collection.Id);
                                }
                            }
                            // No folder found
                            else
                            {
                                Collection collection = fileAdapter.SaveDirectory(gateway.GetDirectoryInfo(), container.Id);
                                fileAdapter.SaveFilesInDirectory(gateway.GetDirectoryInfo(), collection.Id);                                
                            }
                        }
                    }
                }
            });

            string title = "Alerta",
                            message = "La base de datos fue actualizada.";
            MessageDialogStyle style = MessageDialogStyle.Affirmative;

            Library.Model = new ObservableCollection<Plugin>(RepositoryFactory
                    .Create<PluginRepository>()
                    .GetAll()
                );

            MessageDialogResult DialogResult =
                await getMetroWindow().ShowMessageAsync(title, message, style, this.MetroDialogSettings());
        }

        private MetroWindow getMetroWindow()
        {
            return App.Current.MainWindow as MahApps.Metro.Controls.MetroWindow;
        }
    }

}
