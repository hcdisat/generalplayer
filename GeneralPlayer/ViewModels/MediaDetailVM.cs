﻿using GeneralPlayer.Behaviors;
using GeneralPlayer.Core;
using GeneralPlayer.Models;
using System;
using System.Collections.Generic;

namespace GeneralPlayer.ViewModels
{
    public class MediaDetailVM : ViewModelBase<Collection>, IDragable
    {
        private IEnumerable<MediaFile> _mediaCollection;
        private MediaFile _selectedFile;

        public IEnumerable<MediaFile> MediaCollection
        {
            get {
                return _mediaCollection ?? Model.MediaFiles;
            }
            set { SetProperty(ref _mediaCollection, value); }
        }

        public MediaFile SelectedFile
        {
            get { return _selectedFile; }
            set { SetProperty(ref _selectedFile, value); }
        }
        

        public Type DataType
        {
            get { return typeof(MediaFile); }
        }

        public void Remove(object target)
        {
            throw new NotImplementedException();
        }


        public object GetData()
        {
            return SelectedFile;
        }
    }
}
