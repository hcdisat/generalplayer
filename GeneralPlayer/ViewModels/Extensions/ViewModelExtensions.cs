﻿using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralPlayer.ViewModels.Extensions
{
   public static class ViewModelExtensions
    {
       public static MetroDialogSettings MetroDialogSettings(this Core.ViewModelBase vm)
       {
           return new MetroDialogSettings
                    {
                        AffirmativeButtonText = "Si",
                        NegativeButtonText = "No",
                        ColorScheme = MetroDialogColorScheme.Inverted
                    };
       }
    }
}
