﻿using GeneralPlayer.Core;
using GeneralPlayer.Models;
using GeneralPlayer.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GeneralPlayer.ViewModels
{
    public class ManageContainersVM : ViewModelBase<ObservableCollection<Container>>
    {

        public LibraryVM Library { get; set; }

        #region Binding Properties
        private string _containerName;
        public string ContainerName
        {
            get { return _containerName; }
            set { SetProperty(ref _containerName, value); }
        }

        private Container _currentContainer;

        public Container CurrentContainer
        {
            get { return _currentContainer; }
            set { SetProperty(ref _currentContainer, value); }
        }


        private string _path;

        public string Path
        {
            get { return _path; }
            set { SetProperty(ref _path, value); }
        }

        private Plugin _selectedPlugin;

        public Plugin SelectedPlugin
        {
            get { return _selectedPlugin; }
            set { SetProperty(ref _selectedPlugin, value); }
        }

        private string _pluginName;

        public string PluginName
        {
            get { return _pluginName; }
            set { SetProperty(ref _pluginName, value); }
        }

        #endregion

        #region Commands

        private ICommand _deleteCollectionCommand;

        public ICommand DeleteCollectionCommand
        {
            get
            {
                return _deleteCollectionCommand ??
                    (_deleteCollectionCommand = new RelayCommand(() =>
                    {                        
                        RepositoryFactory
                            .Create<ContainerRepository>()
                            .Remove(CurrentContainer);

                        Model.Remove(CurrentContainer);
                        

                    }, () =>
                    {
                        return CurrentContainer != null;
                    }));
            }
        }


        private ICommand _addCollectionCommand;

        public ICommand AddCollectionCommand
        {
            get
            {
                return _addCollectionCommand ??
                    (_addCollectionCommand = new RelayCommand(() =>
                    {
                        if (string.IsNullOrWhiteSpace(ContainerName) && string.IsNullOrWhiteSpace(Path) && SelectedPlugin == null)
                            throw new ArgumentNullException();

                        Model = new ObservableCollection<Container>(
                            RepositoryFactory.Create<ContainerRepository>()
                             .Add(new Container
                             {
                                 Name = ContainerName,
                                 Path = this.Path,
                                 PluginId = SelectedPlugin.Id
                             })
                            );
                        

                    }, () =>
                    {
                        return
                            (!string.IsNullOrWhiteSpace(ContainerName)
                                && !string.IsNullOrWhiteSpace(Path)
                                && SelectedPlugin != null);

                    }));
            }
        }

        private ICommand _fileDialogCommand;

        public ICommand FileDialogCommand
        {
            get
            {
                return _fileDialogCommand ??
                    (_fileDialogCommand = new RelayCommand(() =>
                    {
                        System.Windows.Forms.FolderBrowserDialog dialog =
                            new System.Windows.Forms.FolderBrowserDialog();
                        System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                        if (result == System.Windows.Forms.DialogResult.OK)
                            Path = dialog.SelectedPath;
                    }));
            }
        }

        private ICommand _addPluginCommand;

        public ICommand AddPluginCommand
        {
            get
            {
                return _addPluginCommand ??
                    (_addPluginCommand = new RelayCommand(() =>
                    {
                        if (String.IsNullOrWhiteSpace(PluginName))
                            throw new ArgumentException();
                        Library.Model = RepositoryFactory.Create<PluginRepository>()
                            .Add(new Plugin
                            {
                                Name = PluginName
                            }).ToList();
                    }, () =>
                    {
                        return !string.IsNullOrWhiteSpace(PluginName);
                    }));
            }
        }


        #endregion
      
    }
}
