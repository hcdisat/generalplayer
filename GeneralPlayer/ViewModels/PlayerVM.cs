﻿using Declarations;
using Declarations.Events;
using Declarations.Media;
using Declarations.Players;
using GeneralPlayer.Behaviors;
using GeneralPlayer.Core;
using GeneralPlayer.Models;
using GeneralPlayer.Views;
using Implementation;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Taygeta.Controls;

namespace GeneralPlayer.ViewModels
{
    public class PlayerVM : Core.ViewModelBase<MediaFile>, IDropable
    {
        #region Fields
        private CollapsedPlayerView _collapsedView;
        private IMediaPlayerFactory _mediaPlayerFactory;


        #endregion

        #region Commands
        protected ICommand _playCommand;
        public virtual ICommand PlayCommand
        {
            get
            {
                return _playCommand ??
                    (_playCommand = new RelayCommand(() =>
                    {
                        if (!_player.IsPlaying)
                        {
                            _player.Play();
                            if (IsCollapse)
                                if (_collapsedView == null)
                                    _collapsedView = new CollapsedPlayerView(BrushResource);
                        }
                        else
                            _player.Pause();
                    }, () =>
                    {
                        return _player != null && _player.CurrentMedia != null;
                    }));
            }
        }

        private ICommand _stopCommand;

        public ICommand StopCommand
        {
            get
            {
                return _stopCommand ??
                    (_stopCommand = new RelayCommand(() =>
                    {
                        if (_player.IsPlaying)
                        {
                            Task.Factory.StartNew(() =>
                            {
                                _player.Stop();
                            });
                        }
                    }, () =>
                    {
                        return _player.IsPlaying;
                    }));
            }
        }

        private ICommand _seekCommand;

        public ICommand SeekCommand
        {
            get
            {
                return _seekCommand ??
                    (_seekCommand = new RelayCommand(() =>
                    {
                        _player.Position = (float)TimeSpan.FromSeconds(MediaPosition).Ticks;
                    },
                () =>
                {
                    return IsPlaying;
                }));
            }
        }

        private ICommand _hideControlsCommand;

        public ICommand HideControlsCommand
        {
            get
            {
                return _hideControlsCommand ?? (
                    _hideControlsCommand = new RelayCommand(() =>
                    {
                        if (!IsPlaying)
                        {
                            ControlVisibility = Visibility.Visible;
                            return;
                        }

                        if (ControlVisibility == Visibility.Visible)
                            ControlVisibility = Visibility.Collapsed;
                        else
                            ControlVisibility = Visibility.Visible;

                    }));
            }
        }

        private ICommand _collapsePlayerCommand;

        public ICommand CollapsePlayerCommand
        {
            get
            {
                return _collapsePlayerCommand ??
                    (_collapsePlayerCommand = new RelayCommand(() =>
                    {
                        if (!IsCollapse)
                        {
                            IsCollapse = true;
                            if (_collapsedView == null)
                                _collapsedView = new CollapsedPlayerView(BrushResource);

                            if (_collapsedView.Visibility == Visibility.Collapsed)
                                _collapsedView.Visibility = Visibility.Visible;
                            _collapsedView.Show();
                        }
                        else
                        {
                            if (_collapsedView != null)
                            {
                                _collapsedView.Visibility = Visibility.Collapsed;
                                IsCollapse = false;
                            }
                        }

                    }));
            }
        }

        private ICommand _fullScreenCommand;

        public ICommand FullScreenCommand
        {
            get
            {
                return _fullScreenCommand ??
                    (_fullScreenCommand = new RelayCommand(() =>
                    {
                        if (_collapsedView != null)
                        {
                            if (_collapsedView.WindowState == System.Windows.WindowState.Maximized && _collapsedView.WindowStyle == WindowStyle.None)
                                _collapsedView.WindowState = System.Windows.WindowState.Normal;
                            else
                                _collapsedView.WindowState = System.Windows.WindowState.Maximized;
                        }
                    }));
            }
        }

        #endregion

        #region Queue Management

        private void queueCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    if (e.NewStartingIndex == 0)
                    {
                        Load(Queue[e.NewStartingIndex]);

                        if (!_player.IsPlaying)
                        {
                            PlayCommand.Execute(new object());
                        }
                        MainVm.NextInQueue.Model = Queue[0];
                    }
                    break;

                case System.Collections.Specialized.NotifyCollectionChangedAction.Move:
                    MainVm.NextInQueue.Model = Queue[0];
                    break;

                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                case System.Collections.Specialized.NotifyCollectionChangedAction.Replace:
                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    if (Queue.Count == 0)
                        MainVm.NextInQueue.Model = null;
                    else
                        MainVm.NextInQueue.Model = Queue[0];
                    break; ;
            }
        }

        public ObservableCollection<MediaFile> Queue { get; set; }

        private ICommand _moveUpCommand;

        public ICommand MoveUpCommand
        {
            get
            {
                return _moveUpCommand ??
                    (_moveUpCommand = new RelayCommand<object>((item) =>
                    {
                        int index = getQueueIndex(item);
                        Queue.Move(index, index - 1);

                    }, (item) =>
                    {
                        return getQueueIndex(item) > 0;
                    }));
            }
        }

        private ICommand _moveDownCommand;

        public ICommand MoveDownCommand
        {
            get
            {
                return _moveDownCommand ??
                    (_moveDownCommand = new RelayCommand<object>((item) =>
                    {
                        int index = getQueueIndex(item);
                        Queue.Move(index, index + 1);

                    }, (item) =>
                    {
                        return getQueueIndex(item) < Queue.IndexOf(Queue.LastOrDefault());
                    }));
            }
        }

        private ICommand _deleteCommand;

        public ICommand DeleteCommand
        {
            get
            {
                return _deleteCommand ??
                    (_deleteCommand = new RelayCommand<object>((item) =>
                    {
                        int index = getQueueIndex(item);
                        if (index < 0)
                            throw new InvalidOperationException();
                        Queue.RemoveAt(index);

                    }));
            }
        }

        private int getQueueIndex(object item)
        {
            if (this.Queue == null && this.Queue.Count <= 0)
                return -1;

            var selectedItem = item as MediaFile;
            return selectedItem != null
                ? Queue.IndexOf(selectedItem)
                : -1;
        }

        #endregion

        #region IDropable Implementation
        public Type DataType
        {
            get { return typeof(PlayerVM); }
        }

        public void Drop(object data, int index = -1)
        {
            MediaFile file = data as MediaFile;
            if (file != null)
                Load(file);
        }
        #endregion

        #region Properties
        
        public bool IsAudio
        {
            get
            {
                return
                    (_mediaInfo != null && _mediaInfo.Extension == ".mp3");
            }
        }

        private MediaFile _mediaInfo;

        public MediaFile MediaInfo
        {
            get { return _mediaInfo; }
            set { SetProperty(ref _mediaInfo, value); }
        }


        public VisualBrush BrushResource { get; set; }

        public MainVM MainVm { get; set; }

        private Window _mainWindow;

        public Window MainWindow
        {
            get { return _mainWindow; }
            set { SetProperty(ref _mainWindow, value); }
        }


        private IVideoPlayer _player;
        public IVideoPlayer Player
        {
            get { return _player; }
            set { SetProperty(ref _player, value); }
        }
        private bool _isCollapse;
        public bool IsCollapse
        {
            get { return _isCollapse; }
            set { SetProperty(ref _isCollapse, value); }
        }


        private Uri _nextInQueue;
        public Uri NextInQueue
        {
            get { return _nextInQueue; }
            set { SetProperty(ref _nextInQueue, value); }
        }

        private IMediaFromFile _currentFile;
        public IMediaFromFile CurrentFile
        {
            get
            {
                return _currentFile;

            }
            set
            {
                SetProperty(ref _currentFile, value);

                if (!_player.IsPlaying)
                {
                    _player.Open(_currentFile);
                    _currentFile.Parse(true);
                }
            }
        }

        private string _durationUpstream;

        public string DurationUpstream
        {
            get { return _durationUpstream; }
            set { SetProperty(ref _durationUpstream, value); }
        }

        private string _durationDownstream;

        public string DurationDownstream
        {
            get { return _durationDownstream; }
            set { SetProperty(ref _durationDownstream, value); }
        }

        private bool _isPlaying;
        public bool IsPlaying
        {
            get { return _player.IsPlaying; }
            set { SetProperty(ref _isPlaying, value); }
        }

        private float _mediaPosition;
        public float MediaPosition
        {
            get { return _mediaPosition; }
            set { SetProperty(ref _mediaPosition, value); }
        }
        private Visibility _controlVisibility;

        public Visibility ControlVisibility
        {
            get
            {
                return _controlVisibility;
            }
            set { SetProperty(ref _controlVisibility, value); }
        }

        #endregion

        #region Methods Privates
        private void mediaEnded(object sender, EventArgs e)
        {
            MainWindow.Dispatcher.BeginInvoke(new Action(delegate
            {
                if (Queue.Count > 0)
                {
                    Load(Queue[0]);
                    PlayCommand.Execute(string.Empty);
                    Queue.RemoveAt(0);
                }
                MediaPosition = 0;
            }));
        }

        private void initPlayer()
        {

            _mediaPlayerFactory = new MediaPlayerFactory(true);
            _player = _mediaPlayerFactory.CreatePlayer<IVideoPlayer>();

            _player.Events.MediaEnded += mediaEnded;
            _player.Events.TimeChanged += new EventHandler<MediaPlayerTimeChanged>(eventsDurationChanged);
        }

        private void eventsDurationChanged(object sender, MediaPlayerTimeChanged e)
        {
            MainWindow.Dispatcher.BeginInvoke(new Action(delegate
            {
                TimeSpan up = TimeSpan.FromMilliseconds(e.NewTime);
                TimeSpan down = TimeSpan.FromMilliseconds(Player.Length);
                DurationUpstream = up.ToString(@"hh\:mm\:ss");
                DurationDownstream = "- " + (down - up).ToString(@"hh\:mm\:ss");
                //DurationDownstream = 
            }));
        }

        #endregion

        #region Public Methods
        public PlayerVM()
        {
            //Queue Init
            Queue = Queue ?? new ObservableCollection<MediaFile>();
            Queue.CollectionChanged += queueCollectionChanged;
            initPlayer();
        }

        public void CloseCollapse()
        {
            if (_collapsedView == null)
                return;

            _collapsedView.Close();
            _collapsedView = null;
        }

        public void Load(MediaFile media)
        {
            MediaInfo = media;
            CurrentFile = _mediaPlayerFactory.CreateMedia<IMediaFromFile>(media.Path);
        }
        #endregion
    }
}