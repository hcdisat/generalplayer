﻿using GeneralPlayer.Core;
using GeneralPlayer.Models;
using GeneralPlayer.Models.Repositories;
using MahApps.Metro.Controls.Dialogs;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace GeneralPlayer.ViewModels
{
    public class MainVM : Core.ViewModelBase
    {
        #region ViewModels References

        private PluginRepository _pluginRepo;
        private LibraryVM _library;
        private MediaDetailVM _mediaDetail;
        private PlayerVM _player;
        private NextInQueueVM _nextInQueue;
        private ManageContainersVM _manageContainers;


        private UpdateLibraryVM _updateLibrary;

        public UpdateLibraryVM UpdateLibrary
        {
            get { return _updateLibrary; }
            set { _updateLibrary = value; }
        }

        public ManageContainersVM ManageContainers
        {
            get { return _manageContainers; }
            set { _manageContainers = value; }
        }

        public NextInQueueVM NextInQueue
        {
            get { return _nextInQueue; }
            set { _nextInQueue = value; }
        }


        public PlayerVM Player
        {
            get { return _player; }
            set { SetProperty(ref _player, value); }
        }

        public MediaDetailVM MediaDetail
        {
            get { return _mediaDetail; }
            set { SetProperty(ref _mediaDetail, value); }
        }


        public LibraryVM Library
        {
            get { return _library; }
            set { SetProperty(ref _library, value); }
        }

        #endregion

        #region Properties

        private bool _isRefreshFlyoutOpen;

        public bool IsRefreshFlyoutOpen
        {
            get { return _isRefreshFlyoutOpen; }
            set { SetProperty(ref _isRefreshFlyoutOpen, value); }
        }


        private bool _isSettingsFlyoutOpen;

        public bool IsSettingsFlyoutOpen
        {
            get { return _isSettingsFlyoutOpen; }
            set { SetProperty(ref _isSettingsFlyoutOpen, value); }
        }

        #endregion

        #region Public Methods
        public MainVM(Window mainWindow = null, VisualBrush brush = null)
        {
            _pluginRepo = new PluginRepository();
            _mediaDetail = new MediaDetailVM();
            _nextInQueue = new NextInQueueVM();

            _library = new LibraryVM
            {
                Model = new ObservableCollection<Plugin>(RepositoryFactory
                    .Create<PluginRepository>()
                    .GetAll()
                )
            };

            _updateLibrary = new UpdateLibraryVM {Library = Library };

            _manageContainers = new ManageContainersVM
            {
                Model = new ObservableCollection<Container>(RepositoryFactory
                    .Create<ContainerRepository>()
                    .GetAll()
                 ),

                Library = Library
            };

            mainWindow = mainWindow ?? Application.Current.MainWindow as MainWindow;
            brush = brush ?? new VisualBrush();
            _player = new PlayerVM
            {
                BrushResource = brush,
                MainWindow = mainWindow,
                MainVm = this
            };

        }
        public void SetCurrentPlayingFile()
        {
            if (MediaDetail.SelectedFile != null)
                Player.Load(MediaDetail.SelectedFile);
        }
        #endregion

        #region Commands
        private ICommand _refreshCommand;

        public ICommand RefreshCommand
        {
            get
            {
                return _refreshCommand ??
                (_refreshCommand = new RelayCommand(() =>
                {
                    IsRefreshFlyoutOpen = true;
                }));
            }
        }


        private ICommand _settingsCommand;

        public ICommand SettingsCommand
        {
            get
            {
                return _settingsCommand ??
                    (_settingsCommand = new RelayCommand(() =>
                    {
                        IsSettingsFlyoutOpen = true;
                    }));
            }
        }


        private ICommand _disposeCommand;

        public ICommand DisposeCommand
        {
            get
            {
                return _disposeCommand ??
                    (_disposeCommand = new RelayCommand(() =>
                    {
                        Player.CloseCollapse();
                    }));
            }
        }
        #endregion

    }
}
