﻿
using GeneralPlayer.Core;
using GeneralPlayer.Models;

namespace GeneralPlayer.ViewModels
{
    public class NextInQueueVM : ViewModelBase<MediaFile>
    {
        public NextInQueueVM()
        {

        }

        public NextInQueueVM(MediaFile model)
        {
            Model = model;
        }

        private bool _isNextAvailable;

        public bool IsNextAvailable
        {
            get
            {
                _isNextAvailable = Model != null;
                return _isNextAvailable;
            }
            set { SetProperty(ref _isNextAvailable, value); }
        }
    }
}
