﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace GeneralPlayer.Views
{
    /// <summary>
    /// Lógica de interacción para PlayerViewUserControl.xaml
    /// </summary>
    public partial class PlayerViewUserControl : UserControl
    {
        private VisualBrush _playerBrush;
        private MediaElement _player;
        public PlayerViewUserControl()
        {
            InitializeComponent();
            _playerBrush = this.Resources["Media"] as VisualBrush;
            _player = _playerBrush.Visual as MediaElement;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {            
            if ((_player.Source != null) && (_player.NaturalDuration.HasTimeSpan))
            {
                _seek.Minimum = 0;
                _seek.Maximum = _player.NaturalDuration.TimeSpan.TotalSeconds;
                _seek.Value = _player.Position.TotalSeconds;
            }
        }

        private void onValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (_player.NaturalDuration.HasTimeSpan)
            {
                _fromLeft.Text = TimeSpan.FromSeconds(_seek.Value).ToString(@"hh\:mm\:ss");
                _froRight.Text = "- " + (_player.NaturalDuration.TimeSpan - TimeSpan.FromSeconds(_seek.Value)).ToString(@"hh\:mm\:ss");
            }
        }

        private void _player_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (_player.Position.TotalSeconds == _seek.Value)
            {
                _seek.Value = _seek.Minimum;
                _player.Stop();
            }
        }
    }
}
