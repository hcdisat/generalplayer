﻿using System.Windows;
using System.Windows.Media;

namespace GeneralPlayer.Views
{
    /// <summary>
    /// Lógica de interacción para CollapsedPlayerView.xaml
    /// </summary>
    public partial class CollapsedPlayerView : Window
    {
        public CollapsedPlayerView(VisualBrush brush)
        {
            InitializeComponent();
            MouseDown += (sender, args) => DragMove();
            collapse.Fill = brush;
        }
    }
}
