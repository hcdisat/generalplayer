﻿using Declarations;
using Declarations.Events;
using Declarations.Players;
using Implementation;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using Taygeta.Controls;

namespace GeneralPlayer.Views
{
    /// <summary>
    /// Lógica de interacción para PlayerView.xaml
    /// </summary>
    public partial class PlayerView : System.Windows.Controls.UserControl
    {
        private VideoImageSource _mVideoImage;
        private volatile bool _mIsDrag;



        public IVideoPlayer MPlayer
        {
            get { return (IVideoPlayer)GetValue(MPlayerProperty); }
            set
            { SetValue(MPlayerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MPlayer.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MPlayerProperty =
            DependencyProperty.Register("MPlayer", typeof(IVideoPlayer), typeof(PlayerView), new PropertyMetadata(null));



        public PlayerView()
        {
            InitializeComponent();
            _mVideoImage = ((this.Resources["Media"] as VisualBrush).Visual as VideoImageSource);
        }

        private void InitControls()
        {
            _seek.Value = 0;
            //_froRight.Text = "00:00:00";
            //_fromLeft.Text = "00:00:00";
        }

        private void Events_PlayerPositionChanged(object sender, MediaPlayerPositionChanged e)
        {
            this.Dispatcher.BeginInvoke(new Action(delegate
            {
                if (!_mIsDrag)
                {
                    _seek.Value = (double)e.NewPosition;
                }
            }));
        }

        private void Events_PlayerStopped(object sender, EventArgs e)
        {
            this.Dispatcher.BeginInvoke(new Action(delegate
            {
                if (_mVideoImage != null)
                    //_mVideoImage.Clear();

                InitControls();
            }));
        }

        private void slider2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (MPlayer != null)
            {
                MPlayer.Volume = (int)e.NewValue;
            }
        }

        private void slider1_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            MPlayer.Position = (float)_seek.Value;
            _mIsDrag = false;
        }

        private void slider1_DragStarted(object sender, DragStartedEventArgs e)
        {
            _mIsDrag = true;
        }

        private void _mainPlayer_Loaded(object sender, RoutedEventArgs e)
        {
            MPlayer.Events.PlayerPositionChanged += new EventHandler<MediaPlayerPositionChanged>(Events_PlayerPositionChanged);
            MPlayer.Events.PlayerStopped += new EventHandler(Events_PlayerStopped);

            if (_mVideoImage != null)
                _mVideoImage.Initialize(MPlayer.CustomRendererEx);
        }

    }
}
