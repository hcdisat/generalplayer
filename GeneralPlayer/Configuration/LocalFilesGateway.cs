﻿using GeneralPlayer.Models.Adapters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GeneralPlayer.Configuration
{
    public class LocalFilesGateway :  IContentGateway
    {
        private string _folderContainer;

        private List<DirectoryInfo> _contentDictionary;
        private DirectoryInfo _directoryInfo;
            

        private void buildContent()
        {
            //Implement Threads in this part
            if (!Directory.Exists(_folderContainer))
                throw new DirectoryNotFoundException(
                    string.Format("Directory {0} does not exists", _folderContainer)
                    );

            _contentDictionary = (new DirectoryInfo(_folderContainer))
                .EnumerateDirectories().ToList();

            if (_contentDictionary.Count == 0)
                // No subfolders
                _directoryInfo = new DirectoryInfo(_folderContainer);
        }

        public LocalFilesGateway(string path)
        {
            _folderContainer = path;
            _contentDictionary = new List<DirectoryInfo>();
            buildContent();
        }

        public IEnumerable<DirectoryInfo> GetContent()
        {
            return _contentDictionary;
        }

        public DirectoryInfo GetDirectoryInfo()
        {
            return _directoryInfo;
        }

    }
}
