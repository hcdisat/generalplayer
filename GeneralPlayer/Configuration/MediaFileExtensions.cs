﻿using GeneralPlayer.Models;
using System;
using System.IO;
using System.Linq;

namespace GeneralPlayer.Configuration
{
    public static class MediaFileExtensions
    {
        public static System.Drawing.Bitmap GetVideoThumbnail(this MediaFile media)
        {
            if (media.IsVallidVideoFile())
                throw new NotSupportedException("The File is not a Video File");
            return loadImage(media.Path);
        }

        public static bool IsVallidVideoFile(this MediaFile file)
        {
            return  (Enum.GetNames(typeof(VideoFileExtensions))
                .Any(ex => ex == file.Extension.Replace(".", "")));
        }

        private static System.Drawing.Bitmap loadImage(string outFIleName)
        {
            if (!File.Exists(outFIleName))
                throw new FileNotFoundException();

            var ms = new MemoryStream(File.ReadAllBytes(outFIleName));
            return System.Drawing.Image.FromStream(ms) as System.Drawing.Bitmap;
        }
    }
}
