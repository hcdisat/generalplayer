﻿
namespace GeneralPlayer.Configuration
{
    public enum VideoFileExtensions
    {
        mkv, mp4, avi, flv,
        mpg, mp3
    }
}
