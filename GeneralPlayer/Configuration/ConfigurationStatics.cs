﻿using System;
using System.IO;
using System.Linq;

namespace GeneralPlayer.Configuration
{
    public static class ConfigurationFolders
    {
        // FOLDERS
        public const string BASEFOLDER = "GeneralPlayer";
        public const string CACHE = "Cache";
        public const string THUMBNAILS = "Thumbnails";
        public const string DATABASEFOLDER = "GeneralPlayerData";

        // String Formats
        private const string _simplePattern = @"{0}\{1}";

        public enum Folders { BaseFolder, Cache, Thumbnails, DatabaseFolder }

        public static string GetBaseFolder()
        {
            return
                string.Format(_simplePattern,
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    BASEFOLDER
                );
        }

        public static string GetCacheFolder()
        {
            return
                string.Format(_simplePattern, GetBaseFolder(), CACHE);
        }

        public static string GetThumbnailsFolder()
        {
            return
                string.Format(_simplePattern, GetCacheFolder(), THUMBNAILS);
        }

        public static string GetDatabaseFolder()
        {
            return
                string.Format(_simplePattern,
                   Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                   DATABASEFOLDER
               );
        }

        public static string BuildDirectoryPath(string path, Folders insideFolder)
        {
            string returnBack = string.Empty;
            switch (insideFolder)
            {
                case Folders.BaseFolder:
                    returnBack = string.Format(_simplePattern, GetBaseFolder(), path);
                    break;
                case Folders.Cache:
                    returnBack = string.Format(_simplePattern, GetCacheFolder(), path);
                    break;
                case Folders.Thumbnails:
                    returnBack = string.Format(_simplePattern, GetThumbnailsFolder(), path);
                    break;
                case Folders.DatabaseFolder:
                    returnBack = string.Format(_simplePattern, GetDatabaseFolder(), path);

                    break;
            }
            return returnBack;
        }

        public static class FolderMaker
        {
            private static bool CreateFolder(string path)
            {
                if (Directory.Exists(path))
                    return false;
                var newDir = Directory.CreateDirectory(path);
                return newDir.Exists;
            }

            private static bool CreateFolder(string path, bool overrideFoldersIfExists)
            {
                if (overrideFoldersIfExists)
                    if (!CreateFolder(path))
                    {
                        cleanDirectory(path);
                    }

                return CreateFolder(path);

            }

            private static void cleanDirectory(string path)
            {
                var directoriesInfo = new DirectoryInfo(path);
                var files = directoriesInfo.EnumerateFiles().ToList();
                var directories = directoriesInfo.EnumerateDirectories().ToList();

                if (directories.Count > 0)
                {
                    foreach (DirectoryInfo dir in directories)
                    {
                        cleanDirectory(dir.FullName);
                    }
                }
                else
                {
                    if (files.Count > 0)
                    {
                        foreach (var file in files)
                        {
                            if (File.Exists(file.FullName))
                                File.Delete(file.FullName);
                        }
                    }

                    Directory.Delete(path);
                }
            }

            public static void CreateBaseFolder(bool overrideFolder = false)
            {
                CreateFolder(ConfigurationFolders.GetBaseFolder(), overrideFolder);
            }

            public static void CreateCacheFolder(bool overrideFolder = false)
            {
                CreateFolder(ConfigurationFolders.GetCacheFolder(), overrideFolder);
            }

            public static void CreateThumbnailsFolder(bool overrideFolder = false)
            {
                CreateFolder(GetThumbnailsFolder(), overrideFolder);
            }

            public static void CreateDatabaseFolder(bool overrideFolder = false)
            {
                CreateFolder(ConfigurationFolders.GetDatabaseFolder(), overrideFolder);
            }

            public static void CreateFolders(bool overwrite = false)
            {
                CreateBaseFolder(overwrite);
                CreateCacheFolder(overwrite);
                CreateThumbnailsFolder(overwrite);
                //CreateDatabaseFolder(overwrite);
            }
        }
    }
}
