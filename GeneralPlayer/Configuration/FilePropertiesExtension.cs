﻿using NReco.VideoConverter;
using System;
using System.IO;
using System.Linq;

namespace GeneralPlayer.Configuration
{
    public static class FilePropertiesExtension
    {
        public static string GetProperty(this FileInfo file, FileProperty property)
        {
            Shell32.Folder folder = getShellFolderNameSpace(file.FullName);
            Shell32.FolderItem fItem = folder.ParseName(file.Name);
            return folder.GetDetailsOf(fItem, (int)property);
        }

        public static void GenerateVideoThumbnail(this FileInfo file, string outFIleName = null)
        {
            if (!file.IsVallidVideoFile())
                return;

            if (string.IsNullOrWhiteSpace(outFIleName))
                outFIleName = 
                    ConfigurationFolders.BuildDirectoryPath(
                        file.DirectoryName + @"\" + file.Name + ".jpg", 
                        ConfigurationFolders.Folders.Thumbnails
                    );

            FFMpegConverter ffmpeg = new FFMpegConverter();
            try
            {
                ffmpeg.GetVideoThumbnail(file.FullName, outFIleName, 5);  
            }
            catch (Exception)
            {
                //TODO: Implement some loggin system
                return;
            }           
        }
       
        //Private methods
        private static Shell32.Folder getShellFolderNameSpace(string file)
        {
            Type shellAppType = Type.GetTypeFromProgID("Shell.Application");
            object shell = Activator.CreateInstance(shellAppType);

            return (Shell32.Folder)
                shellAppType.InvokeMember("NameSpace",
                    System.Reflection.BindingFlags.InvokeMethod,
                    null, shell, new object[] { Path.GetDirectoryName(file) });
        }

        public static bool IsVallidVideoFile(this FileInfo file)
        {
            return (Enum.GetNames(typeof(VideoFileExtensions))
                .Any(ex => ex == file.Extension.Replace(".", "")));
        }
    }
}
