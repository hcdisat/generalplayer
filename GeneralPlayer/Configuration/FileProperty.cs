﻿
namespace GeneralPlayer.Configuration
{
    public enum FileProperty
    {
        Name = 0,
        Size = 1,
        ItemType = 2,
        DateModified = 3,
        DateCreated = 4,
        DateAccessed = 5,
        Attributes = 6,
        Availability = 8,
        PerceivedType = 9,
        Owner = 10,
        Kind = 11,
        Rating = 19,
        Length = 27,
        BitRate = 28,
        TotalSize = 50,
        Computer = 54,
        FileExtension = 157,
        Filename = 158,
        SpaceFree = 162,
        Shared = 180,
        FolderName = 183,
        FolderPath = 184,
        Folder = 185,
        Path = 187,
        Type = 189,
        LinkStatus = 195,
        SpaceUsed = 246,
        SharingStatus = 287,
        VideoCompression = 301,
        DataRate = 303,
        FrameHeight = 304,
        FrameRate = 305,
        FrameWidth = 306,
        TotalBitrate = 308
    }
}
