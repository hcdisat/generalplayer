﻿using System;
using System.Windows.Data;

namespace GeneralPlayer.Helpers.Converters
{
    class DoubleToTimeSpanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            long parsedValue = 0;
            long.TryParse(value.ToString(), out parsedValue);
            return TimeSpan.FromTicks(parsedValue);

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
