﻿using System;
using System.Windows;
using System.Windows.Data;

namespace GeneralPlayer.Helpers.Converters
{
    public class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (bool)value
                ? Visibility.Visible
                : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visibility = (Visibility)value;
            bool returnedValue = false;

            switch(visibility)
            {
                case Visibility.Hidden:
                case Visibility.Collapsed:
                    returnedValue = false;
                    break;
                case Visibility.Visible:
                    returnedValue =  true;
                    break;
            }

            return returnedValue;
        }
    }
}
