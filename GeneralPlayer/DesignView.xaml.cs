﻿using Declarations;
using Declarations.Events;
using Declarations.Media;
using Declarations.Players;
using Implementation;
using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;

using GeneralPlayer.ViewModels;
//using GeneralPlayer.Models;
using MahApps.Metro.Controls;


namespace GeneralPlayer
{
    /// <summary>
    /// Lógica de interacción para DesignView.xaml
    /// </summary>
    public partial class DesignView : MetroWindow
    {
        //private IMediaPlayerFactory m_factory;
        //private IVideoPlayer m_player;
        //private IMediaFromFile m_media;
        //private volatile bool m_isDrag;
        //const string FOLDER = @"C:\Users\aniva\Videos\DataVideos";
        ////private ViewModels.PlayerVM playerVm;

        public DesignView()
        {
            InitializeComponent();

            #region Old
            //DataContext = new ViewModels.MainVM();
            //var file = new System.IO.FileInfo(@"C:\Users\aniva\Videos\DataVideos\Aventura\mi corazoncito.mpg");
            //var uri = new Uri(file.FullName, UriKind.Absolute);
            //GeneralPlayerContext db = new GeneralPlayerContext();
            //Random rand = new Random();
            //int seed = rand.Next(200);
            //MediaFile media = db.MediaFiles.Where(m => m.Id == seed).First();
            //Collection collection = db.Collections.FirstOrDefault();
            //_mediaDetail.DataContext = new ViewModels.MediaDetailVM { Model = collection };

            //_nextQueue.DataContext = new ViewModels.NextInQueueVM(media);
            //_list.ItemsSource = db.Plugins.Take(5).ToList();
            //_list.DisplayMemberPath = "Name";

            //var plugin = db.Plugins.Select(p => p).ToList();

            //_plugin.DataContext = new ViewModels.LibraryVM
            //{
            //    Model = plugin
            //};

            //ObservableCollection<MediaFile> files = new ObservableCollection<MediaFile>();
            //var result = (from m in db.MediaFiles
            //              select m).Take(50);

            //foreach (var item in result)
            //    files.Add(item);

            //this.DataContext = new QueueVM
            //{
            //    Queue = files
            //};


            //_player.DataContext = playerVm;

            //_grid.DataContext = media;

            //ThreadPool.QueueUserWorkItem(_ => {
            //    LocalFilesGateway gateway = new LocalFilesGateway(FOLDER);
            //    var folders = gateway.GetContent() as List<System.IO.DirectoryInfo>;

            //    foreach (var folder in folders)
            //    {
            //      var fileAdapter = new LocalFIleAdapter();
            //        Collection collection = fileAdapter.SaveDirectory(folder, 1);
            //        fileAdapter.SaveFilesInDirectory(folder, collection.Id);
            //    }

            //Dispatcher.BeginInvoke(new Action(() =>
            //{
            //    MessageBox.Show("Operations Done!", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            //}));
            //});

            //Plugin data = db.Plugins.FirstOrDefault();
            //Console.WriteLine(data.Containers.FirstOrDefault().Collections.FirstOrDefault().MediaFiles.FirstOrDefault().Name);
            //MediaFile media = db.MediaFiles.FirstOrDefault();

            //MessageBox.Show(media.Collection.Container.Name);
            //var mainVm = new MainVM();
            //this.DataContext = mainVm;

            //Panel p = new Panel();
            //p.BackColor = System.Drawing.Color.Black;
            //windowsFormsHost1.Child = p;

            //m_factory = new MediaPlayerFactory(true);
            //m_player = m_factory.CreatePlayer<IVideoPlayer>();

            //this.DataContext = m_player;

            //m_player.Events.PlayerPositionChanged += new EventHandler<MediaPlayerPositionChanged>(Events_PlayerPositionChanged);
            //m_player.Events.TimeChanged += new EventHandler<MediaPlayerTimeChanged>(Events_TimeChanged);
            //m_player.Events.MediaEnded += new EventHandler(Events_MediaEnded);
            //m_player.Events.PlayerStopped += new EventHandler(Events_PlayerStopped);

            //m_player.WindowHandle = p.Handle;
            //slider2.Value = m_player.Volume;

            #endregion 

            var mainVm = new MainVM(this);
            this.DataContext = mainVm;
        }
        

        //private void Events_PlayerStopped(object sender, EventArgs e)
        //{
        //    this.Dispatcher.BeginInvoke(new Action(delegate
        //    {
        //        InitControls();
        //    }));
        //}

        //private void Events_MediaEnded(object sender, EventArgs e)
        //{
        //    this.Dispatcher.BeginInvoke(new Action(delegate
        //    {
        //        InitControls();
        //    }));
        //}

        //private void InitControls()
        //{
        //    slider1.Value = 0;
        //    label1.Content = "00:00:00";
        //    label3.Content = "00:00:00";
        //}

        //private void Events_TimeChanged(object sender, MediaPlayerTimeChanged e)
        //{
        //    this.Dispatcher.BeginInvoke(new Action(delegate
        //    {
        //        label1.Content = TimeSpan.FromMilliseconds(e.NewTime).ToString().Substring(0, 8);
        //    }));
        //}

        //private void Events_PlayerPositionChanged(object sender, MediaPlayerPositionChanged e)
        //{
        //    this.Dispatcher.BeginInvoke(new Action(delegate
        //    {
        //        if (!m_isDrag)
        //        {
        //            slider1.Value = (double)e.NewPosition;
        //        }
        //    }));
        //}

        //private void button1_Click(object sender, RoutedEventArgs e)
        //{
        //    OpenFileDialog ofd = new OpenFileDialog();
        //    if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        textBlock1.Text = ofd.FileName;
        //        m_media = m_factory.CreateMedia<IMediaFromFile>(ofd.FileName);
        //        m_media.Events.DurationChanged += new EventHandler<MediaDurationChange>(Events_DurationChanged);
        //        m_media.Events.StateChanged += new EventHandler<MediaStateChange>(Events_StateChanged);

        //        m_player.Open(m_media);
        //        m_media.Parse(true);
        //    }
        //}

        //private void button3_Click(object sender, RoutedEventArgs e)
        //{
        //    m_player.Play();
        //}

        //private void Events_StateChanged(object sender, MediaStateChange e)
        //{
        //    this.Dispatcher.BeginInvoke(new Action(delegate
        //    {

        //    }));
        //}

        //private void Events_DurationChanged(object sender, MediaDurationChange e)
        //{
        //    this.Dispatcher.BeginInvoke(new Action(delegate
        //    {
        //        label3.Content = TimeSpan.FromMilliseconds(e.NewDuration).ToString().Substring(0, 8);
        //    }));
        //}

        //private void button2_Click(object sender, RoutedEventArgs e)
        //{
        //    m_player.Pause();
        //}

        //private void button4_Click(object sender, RoutedEventArgs e)
        //{
        //    m_player.Stop();
        //}

        //private void button5_Click(object sender, RoutedEventArgs e)
        //{
        //    m_player.ToggleMute();
        //}

        //private void slider2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{
        //    if (m_player != null)
        //    {
        //        m_player.Volume = (int)e.NewValue;
        //    }
        //}

        //private void slider1_DragCompleted(object sender, DragCompletedEventArgs e)
        //{
        //    m_player.Position = (float)slider1.Value;
        //    m_isDrag = false;
        //}

        //private void slider1_DragStarted(object sender, DragStartedEventArgs e)
        //{
        //    m_isDrag = true;
        //}

        ////    private async void OnLoaded(object sender, RoutedEventArgs e)
        ////    {
        ////        #region Notifications Test
        ////        MetroDialogSettings settings = new MetroDialogSettings{
        ////            AffirmativeButtonText = "Aceptar",
        ////            NegativeButtonText = "Cancelar",
        ////            ColorScheme = MetroDialogColorScheme.Inverted                
        ////        };

        ////        MessageDialogResult dialogResult = 
        ////            await this.ShowMessageAsync("Alerta", "¿Desea detener la reproducción actual y reproducir el nuevo elemento?",
        ////            MessageDialogStyle.Affirmative, settings);

        ////        //dialogResult.Start();
        ////        //dialogResult.Wait();

        ////        if (dialogResult == MessageDialogResult.Affirmative)
        ////            await this.ShowMessageAsync("The Result", "You Pressed \"Aceptar\"", MessageDialogStyle.Affirmative, settings);
        ////        else
        ////            await this.ShowMessageAsync("The Result", "You Pressed \"Cancelar\"", MessageDialogStyle.Affirmative, settings);

        ////        #endregion
        ////    }
        ////}

        ////class QueueVM : Core.ViewModelBase
        ////{
        ////    public ObservableCollection<MediaFile> Queue { get; set; }

        ////    private ICommand _moveUpCommand;

        ////    public ICommand MoveUpCommand
        ////    {
        ////        get
        ////        {
        ////            return _moveUpCommand ??
        ////                (_moveUpCommand = new RelayCommand<object>((item) =>
        ////                {
        ////                    int index = getQueueIndex(item);
        ////                    Queue.Move(index, index - 1);

        ////                }, (item) =>
        ////                {
        ////                    return getQueueIndex(item) > 0;
        ////                }));
        ////        }
        ////    }

        ////    private ICommand _moveDownCommand;

        ////    public ICommand MoveDownCommand
        ////    {
        ////        get
        ////        {
        ////            return _moveDownCommand ??
        ////                (_moveDownCommand = new RelayCommand<object>((item) =>
        ////                {

        ////                    int index = getQueueIndex(item);
        ////                    Queue.Move(index, index + 1);

        ////                }, (item) =>
        ////                {
        ////                    return getQueueIndex(item) < Queue.IndexOf(Queue.LastOrDefault());
        ////                }));
        ////        }
        ////    }

        ////    private ICommand _deleteCommand;

        ////    public ICommand DeleteCommand
        ////    {
        ////        get
        ////        {
        ////            return _deleteCommand ??
        ////                (_deleteCommand = new RelayCommand<object>((item) => {
        ////                    int index = getQueueIndex(item);
        ////                    if (index < 0)
        ////                        throw new InvalidOperationException();
        ////                    Queue.RemoveAt(index);

        ////                }));
        ////        }
        ////    }



        ////    private int getQueueIndex(object item)
        ////    {
        ////        if (this.Queue == null && this.Queue.Count <= 0)
        ////            return -1;

        ////        var selectedItem = item as MediaFile;
        ////        return selectedItem != null
        ////            ? Queue.IndexOf(selectedItem)
        ////            : -1;
        ////    }

        ////}


    }
}
