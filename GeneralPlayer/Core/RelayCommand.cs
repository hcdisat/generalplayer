﻿using System;

using System.Windows.Input;

namespace GeneralPlayer.Core
{
    public class RelayCommand<T> : ICommand
    {

        private readonly Action<T> _executeField;
        private readonly Func<T, bool> _canExecuteField;

        private static bool canExecute(T parameter)
        {
            return true;
        }

        /// <summary>
        /// Casts the commad paramenter to the correct type, it sound simple but there's a problem if the type
        /// is a Enum. so lets try this
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private T translateParameter(object parameter)
        {
            return (parameter != null) && (typeof(T).IsEnum)
                ? (T)Enum.Parse(typeof(T), (string)parameter)
                : (T)parameter;
        }

        public RelayCommand(Action<T> execute, Func<T, bool> canExecuteParam = null)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");
            _executeField = execute;
            _canExecuteField = canExecuteParam ?? canExecute;
        }

        #region ICommand Implementation
        public bool CanExecute(object parameter)
        {
            return
                _canExecuteField(translateParameter(parameter));
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (_canExecuteField != null)
                {
                    CommandManager.RequerySuggested += value;
                }
            }
            remove
            {
                if (_canExecuteField != null)
                {
                    CommandManager.RequerySuggested -= value;
                }
            }
        }

        public void Execute(object parameter)
        {
            _executeField(translateParameter(parameter));
        }
        #endregion
    }

    public class RelayCommand : RelayCommand<object>
    {
        public RelayCommand(Action execute, Func<bool> canExecute = null)
            : base(obj => execute(),
                (canExecute == null ? null : 
            new Func<object, bool>(obj => canExecute()))) {
           
        }
    }
}
