﻿using System.Windows;

namespace GeneralPlayer.Core.Behaviors
{
    public interface IBehavior
    {
        void OnEnabled(DependencyObject dependecyObject);
        void OnDisabling(DependencyObject dependecyObject);
    }
}
