﻿using System.Windows;

namespace GeneralPlayer.Core.Behaviors
{
    public static class AttachedBehaviors
    {
        public static readonly DependencyProperty BehaviorProperty = 
            DependencyProperty.RegisterAttached(
                "Behavior", typeof(IBehavior), typeof(AttachedBehaviors), new UIPropertyMetadata(null)
                );

        public static readonly DependencyProperty IsEnabledProperty = 
            DependencyProperty.RegisterAttached(
            "IsEnabled", typeof(bool), typeof(AttachedBehaviors), new UIPropertyMetadata(false, OnIsEnabledChanged)
            );

        private static void OnIsEnabledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d == null)
                return;
            IBehavior behavior = GetBehavior(d);

            if (behavior == null)
                return;

            if ((bool)e.NewValue)
                behavior.OnEnabled(d);
            else
                behavior.OnDisabling(d);
        }

        private static IBehavior GetBehavior(DependencyObject dependencyObject)
        {
            return dependencyObject.GetValue(BehaviorProperty) as IBehavior;
        }

        private static void SetBehavior(DependencyObject dependencyObject, IBehavior value)
        {
            dependencyObject.SetValue(BehaviorProperty, value);
        }

        public static bool GetIsEnabled(DependencyObject dependencyObject)
        {
            return (bool)dependencyObject.GetValue(IsEnabledProperty);
        }

        public static void SetIsEnabled(DependencyObject dependencyObject, bool value)
        {
            dependencyObject.SetValue(IsEnabledProperty, value);
        }
    }
}
