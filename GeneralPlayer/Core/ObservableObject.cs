﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GeneralPlayer.Core
{
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChange(string propertyNName)
        {
            System.Diagnostics.Debug.Assert(GetType().GetProperty(propertyNName) != null);

            var pc = PropertyChanged;
            if (pc != null)
                pc(this, new PropertyChangedEventArgs(propertyNName));
        }

        protected bool SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if ( !EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                OnPropertyChange(propertyName);
                return true;
            }

            return false;
        }
    }
}
