﻿using GeneralPlayer.Models;
using GeneralPlayer.ViewModels;
using System.Windows;
using System.Windows.Interactivity;

namespace GeneralPlayer.Behaviors
{
    public class DatagridBehabior : Behavior<System.Windows.Controls.DataGrid>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(selectionChanged);
        }

        private void selectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectedFile = AssociatedObject.SelectedItem as MediaFile;
            if (selectedFile == null) return;
            MainVM mainVm = Application.Current.MainWindow.DataContext as MainVM;
            mainVm.MediaDetail.SelectedFile = selectedFile;
        }
    }
}
