﻿
using GeneralPlayer.ViewModels;
using MahApps.Metro.Controls.Dialogs;
using System.Windows;
using System.Windows.Interactivity;

namespace GeneralPlayer.Behaviors
{
    public class NotificationBehavior : Behavior<FrameworkElement>
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public MessageDialogStyle Style { get; set; }

        public MessageDialogResult DialogResult { get; private set; }

        private MetroDialogSettings _settings;

        public MetroDialogSettings Settings
        {
            get
            {
                return _settings ??
                    (_settings = new MetroDialogSettings
                    {
                        AffirmativeButtonText = "Si",
                        NegativeButtonText = "No",
                        ColorScheme = MetroDialogColorScheme.Inverted
                    });
            }
            set { _settings = value; }
        }

        #region Constructors
        public NotificationBehavior()
        {

        }
        public NotificationBehavior(string title, string message)
        {
            Title = title;
            Message = message;
        }

        public NotificationBehavior(string title, string message, MessageDialogStyle style)
            : this(title, message)
        {
            Style = style;
        }

        public NotificationBehavior(string title, string message, MetroDialogSettings settings)
            : this(title, message)
        {
            Settings = settings;
        }

        public NotificationBehavior(string title, string message, MessageDialogStyle style, MetroDialogSettings settings)
            : this(title, message, style)
        {
            Settings = settings;
        }

        #endregion
        protected override void OnAttached()
        {
            AssociatedObject.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(doubleClick);
        }

        private async void doubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                var mainW = Application.Current.MainWindow as MainWindow;
                var mainVM = mainW.DataContext as MainVM;

                if (mainVM.Player.IsPlaying)
                {
                    DialogResult = await mainW.ShowMessageAsync(Title, Message, Style, Settings);
                    if (DialogResult == MessageDialogResult.Negative)
                        e.Handled = true; 
                }
            }
        }

    }
}
