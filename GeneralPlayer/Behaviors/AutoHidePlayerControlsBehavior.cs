﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace GeneralPlayer.Behaviors
{
    public class AutoHidePlayerControlsBehavior : Behavior<FrameworkElement>
    {
        private bool _isVisible;
        protected override void OnAttached()
        {
            base.OnAttached();
            ////AssociatedObject.MouseEnter += new MouseEventHandler(onMouseEnter);
            //AssociatedObject.Mou += new MouseEventHandler(onMouseLeave);
            AssociatedObject.PreviewMouseMove += new MouseEventHandler(onMouseEnter);
        }

        private void onMouseEnter(object sender, MouseEventArgs e)
        {
            if (!_isVisible)
            {
                Stopwatch timer = new Stopwatch();
                _isVisible = true;
                setVisibility(_isVisible);
                timer.Start();
                while (timer.ElapsedMilliseconds < 3000)
                {
                    ;
                }
                timer.Reset();

            }
        }

        private void onMouseLeave(object sender, MouseEventArgs e)
        {
            if (_isVisible)
            {
                _isVisible = false;
                setVisibility(_isVisible);
            } 
        }

        private void setVisibility(bool visibility)
        {
            //if (_mainVm == null)
            //    _mainVm = Application.Current.MainWindow.DataContext as ViewModels.MainVM;

            //_mainVm.Player.ControlVisibility = visibility;
            //MessageBox.Show(visibility.ToString());
        }
    }
}
