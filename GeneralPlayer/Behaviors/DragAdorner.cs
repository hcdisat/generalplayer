﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace GeneralPlayer.Behaviors
{
    public class DragAdorner : Adorner
    {
        private AdornerLayer _adornerLayer;

        public DragAdorner(UIElement adornedElement) : base(adornedElement)
        {
            _adornerLayer = AdornerLayer.GetAdornerLayer(adornedElement);
            _adornerLayer.Add(this);
        }

        public void Update()
        {
            _adornerLayer.Update(this.AdornedElement);
            this.Visibility = Visibility.Visible;
        }

        public void Remove()
        {
            this.Visibility = Visibility.Collapsed;
        }

        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            //base.OnRender(drawingContext);
            Rect adornedElementRect = new Rect(this.AdornedElement.DesiredSize);

            SolidColorBrush renderBrush = new SolidColorBrush(Colors.Red);
            renderBrush.Opacity = 0.5;
            Pen renderPen = new Pen(new SolidColorBrush(Colors.White), 1.5);
            double renderRadio = 5.0;

            drawingContext.DrawEllipse(renderBrush, renderPen, adornedElementRect.TopLeft, renderRadio, renderRadio);
            drawingContext.DrawEllipse(renderBrush, renderPen, adornedElementRect.TopRight, renderRadio, renderRadio);
            drawingContext.DrawEllipse(renderBrush, renderPen, adornedElementRect.BottomLeft, renderRadio, renderRadio);
            drawingContext.DrawEllipse(renderBrush, renderPen, adornedElementRect.BottomRight, renderRadio, renderRadio);
        }
    }
}
