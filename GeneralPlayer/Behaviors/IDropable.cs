﻿using System;

namespace GeneralPlayer.Behaviors
{
    public interface IDropable
    {
        /// <summary>
        /// Type of the data item
        /// </summary>
        Type DataType { get; }

        /// <summary>
        /// Drop data into the collection
        /// </summary>
        /// <param name="data"></param>
        /// <param name="index"></param>
        void Drop(object data, int index = -1);
    }
}
