﻿using System;

namespace GeneralPlayer.Behaviors
{
    public interface IDragable
    {
        /// <summary>
        /// Type of the data item
        /// </summary>
        Type DataType { get; }

        /// <summary>
        /// Remove the object from the draged collection
        /// </summary>
        /// <param name="target"></param>
        void Remove(object target);

        object GetData();
    }
}
