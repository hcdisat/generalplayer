﻿using GeneralPlayer.Models;
using System.Windows;
using System.Windows.Interactivity;

namespace GeneralPlayer.Behaviors
{
    public class DragBehavior : Behavior<FrameworkElement>
    {
        private bool _isMouseClicked;

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.PreviewMouseLeftButtonDown +=
                new System.Windows.Input.MouseButtonEventHandler(mouseLeftButtonDown);

            AssociatedObject.PreviewMouseLeftButtonUp +=
                new System.Windows.Input.MouseButtonEventHandler(mouseLeftButtonUp);

            AssociatedObject.MouseLeave += new System.Windows.Input.MouseEventHandler(mouseLeave);
        }

        private void mouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (_isMouseClicked)
            {
                // Verify if  the assosiated abject implemets the interface to be draged
                IDragable dragObject = AssociatedObject.DataContext as IDragable;
                if (dragObject != null)
                {
                    DataObject data = new DataObject();
                    var media = dragObject.GetData() as MediaFile;
                    if (media != null)
                    {
                        data.SetData(dragObject.DataType, media);
                        System.Windows.DragDrop.DoDragDrop(AssociatedObject, data, DragDropEffects.Copy); 
                    }
                }
            }
            _isMouseClicked = false;
        }

        private void mouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _isMouseClicked = false;
        }

        private void mouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _isMouseClicked = true;

            //DataGrid dgv = sender as DataGrid;
            //MainVM mainVm = Application.Current.MainWindow.DataContext as MainVM;
            //MessageBox.Show(dgv.SelectedIndex.ToString());
            //mainVm.MediaDetail.SelectedFile = dgv.SelectedItem as MediaFile;
        }
    }
}
