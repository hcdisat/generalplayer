﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Interactivity;

namespace GeneralPlayer.Behaviors
{
    public abstract class FilterBehaviorBase<TModel> : Behavior<FrameworkElement>
    {



        public IEnumerable<TModel> DataCollection
        {
            get { return (IEnumerable<TModel>)GetValue(DataCollectionProperty); }
            set { SetValue(DataCollectionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DataCollection.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DataCollectionProperty =
            DependencyProperty.Register("DataCollection", typeof(IEnumerable<TModel>),
            typeof(FilterBehaviorBase<TModel>), new PropertyMetadata(default(IEnumerable<TModel>)));

    }
}
