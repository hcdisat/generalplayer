﻿using GeneralPlayer.Models;
using GeneralPlayer.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace GeneralPlayer.Behaviors
{
    public class SelectCollectionBehavior : Behavior<TreeView>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectedItemChanged += new RoutedPropertyChangedEventHandler<object>(onSelectedItemChanged);
        }

        private void onSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var collection = AssociatedObject.SelectedItem as Collection;
            if (collection == null)
                return;

            var parent = Application.Current.MainWindow;
            var mainVm = parent.DataContext as MainVM;
            mainVm.MediaDetail.Model = collection;
        }
    }
}
