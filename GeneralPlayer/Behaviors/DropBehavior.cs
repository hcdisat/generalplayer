﻿using GeneralPlayer.ViewModels;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace GeneralPlayer.Behaviors
{
    public class DropBehavior : Behavior<FrameworkElement>
    {
        private Type _dataType;
        private DragAdorner _adorner;

        public FrameworkElement Target { get; set; }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.AllowDrop = true;
            AssociatedObject.DragEnter += new DragEventHandler(dragEnter);
            AssociatedObject.DragOver += new DragEventHandler(dragOver);
            AssociatedObject.DragLeave += new DragEventHandler(dragLeave);
            AssociatedObject.Drop += new DragEventHandler(drop);
        }

        private void drop(object sender, DragEventArgs e)
        {
            MainVM mainVm = Application.Current.MainWindow.DataContext as MainVM;
            DataGrid dataGrid = AssociatedObject as DataGrid;
            if (dataGrid != null)
            {
                dataGrid.ItemsSource = mainVm.Player.Queue;
                var mediaFile = e.Data.GetData(typeof(Models.MediaFile)) as GeneralPlayer.Models.MediaFile;
                mainVm.Player.Queue.Add(mediaFile);
            }
            else
                mainVm.SetCurrentPlayingFile();

            if (_adorner != null)
                _adorner.Remove();

            e.Handled = true;
        }

        private void dragLeave(object sender, DragEventArgs e)
        {
            if (_adorner != null)
                _adorner.Remove();
            e.Handled = true;
        }

        private void dragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(GeneralPlayer.Models.MediaFile)))
            {
                setDragAndDropEffect(e);

                if (_adorner != null)
                    _adorner.Update();
            }
            e.Handled = true;
        }

        private void setDragAndDropEffect(DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            if (e.Data.GetDataPresent(_dataType))
                e.Effects = DragDropEffects.Copy;
        }

        private void dragEnter(object sender, DragEventArgs e)
        {

            if (_dataType == null)
            {
                MainVM mainVm = Application.Current.MainWindow.DataContext as MainVM;
                if (mainVm == null) return;
                _dataType = mainVm.MediaDetail.DataType;
            }

            if (_adorner == null)
                _adorner = new DragAdorner(sender as UIElement);

            e.Handled = true;
        }
    }
}
