﻿using GeneralPlayer.Models;
using GeneralPlayer.ViewModels;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using GeneralPlayer.ViewModels.Extensions;

namespace GeneralPlayer.Behaviors
{
    public class DoubleClickPlayBehavior : Behavior<FrameworkElement>
    {
        protected override void OnAttached()
        {
            AssociatedObject.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(doubleClickPlay);
        }

        private void doubleClickPlay(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)   // Checks For doubleclick action
            {
                var dGrid = AssociatedObject as DataGrid;
                if (dGrid.SelectedIndex >= 0)
                {
                    //var window = Application.Current.MainWindow as MainWindow;
                    var window = Application.Current.MainWindow;
                    var mainVm = window.DataContext as MainVM;

                    // handle plays the media
                    Action<bool> play = new Action<bool>((forcePlay) =>
                    {
                        var media = dGrid.SelectedItem as MediaFile;
                       
                        // handle if is a a first play or if is playing
                        Action safePlay = () =>
                        {
                            mainVm.Player.Load(media);
                            mainVm.Player.Queue.Remove(media);

                            mainVm.Player.PlayCommand.Execute(string.Empty);
                        };

                        if (forcePlay)
                            Task.Factory.StartNew(() =>
                            {
                                mainVm.Player.Player.Stop();
                                safePlay();
                            });
                        else
                            safePlay();
                    });


                    if (!mainVm.Player.IsPlaying)
                        play(false);
                    else
                    {
                        // Handles Notificacion
                        var action = new Action(async () => {
                            string title = "Alerta",
                            message = "¿Desea detener la reproducción actual y reproducir el nuevo elemento?";
                            MessageDialogStyle style = MessageDialogStyle.AffirmativeAndNegative;

                            MessageDialogResult DialogResult =
                                await (window as MahApps.Metro.Controls.MetroWindow).ShowMessageAsync(title, message, style, mainVm.MetroDialogSettings());
                            if (DialogResult == MessageDialogResult.Negative)
                                e.Handled = true;
                            else
                                play(true);
                        });
                        action();
                    }
                }
            }
        }
    }
}
