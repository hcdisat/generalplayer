﻿using GeneralPlayer.Models;
using System.Windows.Controls;
using System.Windows.Data;

namespace GeneralPlayer.Behaviors
{
    class MediaFilesFilterBehavior : FilterBehaviorBase<MediaFile>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.KeyDown += new System.Windows.Input.KeyEventHandler(keyDown);
            AssociatedObject.KeyUp += new System.Windows.Input.KeyEventHandler(keyDown);
        }

        private void keyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var input = sender as TextBox;
            var view = CollectionViewSource.GetDefaultView(DataCollection);

            if (string.IsNullOrWhiteSpace(input.Text))
                view.Filter = null;
            else
                view.Filter = obj => ((MediaFile)obj).Name
                    .ToLower()
                    .Contains(input.Text.ToLower());
        }
    }
}
