﻿using System.Collections.Generic;

namespace GeneralPlayer.Models
{
    public class Container : IModel
    {
        public long PluginId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }


        public virtual Plugin Plugin { get; set; }
        public virtual ICollection<Collection> Collections { get; set; }

        public Container()
        {
            Collections = new HashSet<Collection>();
        }

    }
}
