﻿using System.ComponentModel.DataAnnotations;
namespace GeneralPlayer.Models
{
    public abstract class IModel: GeneralPlayer.Core.ObservableObject
    {
        private long _id;

        [Key]
        public long Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }
    }
}
