﻿using System.Collections.Generic;

namespace GeneralPlayer.Models
{
    public class Plugin : IModel
    {
        public string Name { get; set; }

        public virtual ICollection<Container> Containers { get; set; }

        public Plugin()
        {
            Containers = new HashSet<Container>();
        }
    }
}
