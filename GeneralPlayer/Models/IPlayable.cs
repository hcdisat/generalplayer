﻿using System;

namespace GeneralPlayer.Models
{
    public interface IPlayable
    {
        Uri UriResource { get; }
    }
}
