﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.Linq;

namespace GeneralPlayer.Models.Repositories
{
    public abstract class RepositoryBase<TModel> : IDisposable, IRepository<TModel> where TModel : IModel
    {
        private GeneralPlayerContext _context;
        private GeneralPlayerContext.EntityEnum _modelIdentifier;
        private DbSet<TModel> _model;
        private bool _disposed = false;

        public RepositoryBase()
        {
            _context = GeneralPlayerContext.GetInstance();
            init();
        }

        private void init()
        {
            getModel();
            _model = _context[_modelIdentifier] as DbSet<TModel>;
        }

        public RepositoryBase(string databasePath)
        {
            SetSQLiteConnection(databasePath);            
        }

        public void SetSQLiteConnection(string databasePath)
        {
            if ( !string.IsNullOrWhiteSpace(databasePath) )
            {
                string conString = @"Data Source=|DataDirectory|{0}";
                SQLiteConnection sqliteConnection =
                    new SQLiteConnection()
                    {
                        ConnectionString = string.Format(conString, databasePath)
                    };
                _context = GeneralPlayerContext.GetInstance(sqliteConnection, true); 
                
            }
            init();
        }

        public virtual IEnumerable<TModel> GetAll()
        {
            return from m in _model
                   select m;
        }

        public virtual TModel GetById(long id)
        {
            return (from m in _model
                   where m.Id == id
                   select m).FirstOrDefault();
        }

        public virtual void Remove(TModel model)
        {            
            _model.Remove(model);
            _context.SaveChanges();
        }

        public virtual IEnumerable<TModel> Add(TModel model)
        {            
            _model.Add(model);
            _context.SaveChanges();
            return GetAll();
        }

        public virtual IEnumerable<TModel> Update(TModel model)
        {
            var target = GetById(model.Id);
            target = model;
            _context.SaveChanges();
            return GetAll();
        }

        private void getModel()
        {
            Enum.TryParse(typeof(TModel).Name, out _modelIdentifier);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if(disposing)
                    if(_context != null)
                        _context.Dispose();
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            //GC.SuppressFinalize(this);
        }


        public virtual void Truncate()
        {
            _model.RemoveRange(GetAll());
            _context.SaveChanges();
        }
    }
}
