﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralPlayer.Models.Repositories
{
    public static class RepositoryFactory
    {
        public static T Create<T>()
        {
            return Activator.CreateInstance<T>();
        }
    }
}
