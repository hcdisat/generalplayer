﻿
namespace GeneralPlayer.Models.Repositories
{
    public class ContainerRepository : RepositoryBase<Container>
    {
        public ContainerRepository()
        {

        }

        public ContainerRepository(string databasePath)
            : base(databasePath)
        {

        }
    }
}
