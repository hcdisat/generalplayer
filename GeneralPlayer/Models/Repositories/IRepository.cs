﻿using System.Collections.Generic;

namespace GeneralPlayer.Models.Repositories
{
    public interface IRepository<TModel>
    {
         IEnumerable<TModel> GetAll();
        
         TModel GetById(long id);

         void Remove(TModel model);

         IEnumerable<TModel> Add(TModel model);

         IEnumerable<TModel> Update(TModel model);

         void Truncate();

    }
}
