﻿using System.Data.Entity;
using System.Data.SQLite;

namespace GeneralPlayer.Models
{
    public class GeneralPlayerContext : DbContext
    {
        private static GeneralPlayerContext instance;

        public DbSet<Collection> Collections { get; set; }
        public DbSet<MediaFile> MediaFiles { get; set; }
        public DbSet<Plugin> Plugins { get; set; }

        public DbSet<Container> Containers { get; set; }

        private GeneralPlayerContext()
        {
            Database.SetInitializer<GeneralPlayerContext>(null);
        }

        private GeneralPlayerContext(SQLiteConnection conexion, bool par) :base(conexion, par)
        {
            Database.SetInitializer<GeneralPlayerContext>(null);
        }

        public static GeneralPlayerContext GetInstance() 
        {
            return instance ??
                (instance = new GeneralPlayerContext());
        }

        public static GeneralPlayerContext GetInstance(SQLiteConnection conextion, bool parameners)
        {
            return instance ??
                (instance = new GeneralPlayerContext(conextion, parameners));
        }

        public object this[EntityEnum index]
        {
            get
            {
                switch (index)
                {
                    case EntityEnum.Collection:
                        return Collections;
                    case EntityEnum.MediaFile:
                        return MediaFiles;
                    case EntityEnum.Plugin:
                        return Plugins;
                    case EntityEnum.Container:
                        return Containers;
                    default:
                        return null;
                }
            }
        }

        public enum EntityEnum
        {
            Collection,
            MediaFile,
            Plugin,
            Container
        }
    }

}
