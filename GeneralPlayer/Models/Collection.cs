﻿using System.Collections.Generic;

namespace GeneralPlayer.Models
{
    public class Collection : IModel
    {
        public long ContainerId { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public int FilesCount { get; set; }

        public virtual ICollection<MediaFile> MediaFiles { get; set; }
        public virtual Container Container { get; set; }


        public Collection()
        {
            MediaFiles = new HashSet<MediaFile>();
        }
    }
}
