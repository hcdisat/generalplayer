﻿using System;

namespace GeneralPlayer.Models
{
    public class MediaFile : IModel, IPlayable
    {
        public long CollectionId { get; set; }
        public string Name { get; set; }
        public double Duration { get; set; }
        public string Path { get; set; }
        public string Extension { get; set; }

        public string Image { get; set; }

        public virtual Collection Collection { get; set; }

        public Uri UriResource
        {
            get { return new Uri(Path, UriKind.Absolute); }
        }

        private System.Windows.Input.ICommand _moveUpCommand;
        public System.Windows.Input.ICommand MoveUpCommand
        {
            get
            {
                return _moveUpCommand ??
                    (_moveUpCommand = new GeneralPlayer.Core.RelayCommand<object>((item) =>
                    {
                        var selectedItem = item as MediaFile;
                        System.Windows.MessageBox.Show(selectedItem.Name);
                    }, (item) =>
                    {
                        //return this.Queue != null && this.Queue.Count > 0;
                        return true;
                    }));
            }
        }
    }
}
