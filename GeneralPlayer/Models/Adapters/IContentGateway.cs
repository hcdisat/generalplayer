﻿using System.Collections.Generic;
namespace GeneralPlayer.Models.Adapters
{
    interface IContentGateway
    {
        IEnumerable<System.IO.DirectoryInfo> GetContent();
    }
}
