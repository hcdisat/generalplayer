﻿using GeneralPlayer.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace GeneralPlayer.Models.Adapters
{
    public class LocalFIleAdapter : AdapterBase
    {
        /// <summary>
        /// Save a single directory metadata to the database
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="containerId"></param>
        /// <returns>Collection</returns>
        public Collection SaveDirectory(DirectoryInfo directory, long containerId)
        {
            //Collection newCollection = newCollection = new Collection
            //{
            //    ContainerId = containerId,
            //    Name = directory.Name,
            //    Source = directory.FullName,
            //    FilesCount = directory.EnumerateFiles().Count()
            //};
            //var context = getContext();
            //context.Collections.Add(newCollection);
            //context.SaveChanges();

            return Repositories.RepositoryFactory
                .Create<Repositories.CollectionRepository>()
                .Add(new Collection
                    {
                        ContainerId = containerId,
                        Name = directory.Name,
                        Source = directory.FullName,
                        FilesCount = directory.EnumerateFiles().Count()
                    }).LastOrDefault();
        }

        /// <summary>
        /// Saves all files contained in a folder to the database
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="collectionId"></param>
        public void SaveFilesInDirectory(DirectoryInfo directory, long collectionId)
        {
            IEnumerable<DirectoryInfo> subFolders = directory.EnumerateDirectories();
            if (subFolders.Count() > 0)
            {
                foreach (var folder in directory.EnumerateDirectories())
                    SaveFilesInDirectory(folder, collectionId);
            }

            foreach (var file in directory.EnumerateFiles())
                SaveFile(file, collectionId);
        }

        /// <summary>
        /// Save a single file metadata in a directory to the database
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="collectionId"></param>
        /// <returns>MediaFile</returns>
        public MediaFile SaveFile(FileInfo file, long collectionId)
        {
            if (file.IsVallidVideoFile())
            {
                var context = getContext();
                MediaFile media = new MediaFile();
                TimeSpan outValue = new TimeSpan();
                media.CollectionId = collectionId;
                media.Name = file.Name;
                media.Extension = file.Extension;
                media.Path = file.FullName;

                string dir = ConfigurationFolders.BuildDirectoryPath(file.Directory.Name,
                        ConfigurationFolders.Folders.Thumbnails);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                media.Image = dir + @"\" + file.Name + ".jpg";

                if (TimeSpan.TryParse(file.GetProperty(FileProperty.Length), out outValue))
                    media.Duration = outValue.Ticks;
                context.MediaFiles.Add(media);
                file.GenerateVideoThumbnail(media.Image);
                saveContextAsync();
                return media;
            }
            return null;
        }


    }
}
