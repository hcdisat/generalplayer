﻿using System.Threading.Tasks;

namespace GeneralPlayer.Models.Adapters
{
    public abstract class AdapterBase
    {

        /// <summary>
        /// Get or Initialize the database context
        /// </summary>
        /// <returns>GeneralPlayerContext</returns>
        public GeneralPlayerContext getContext()
        {
            return GeneralPlayerContext.GetInstance();
        }

        protected Task<int> saveContextAsync()
        {
            return getContext().SaveChangesAsync();
        }
    }
}
