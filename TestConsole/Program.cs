﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using GeneralPlayer.Models;
using GeneralPlayer.Configuration;
using System.Threading;
using GeneralPlayer.Models.Adapters;
using GeneralPlayer.Models.Repositories;
namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            const string  DATABASENAME = "GeneralPlayer.sqlite";
            //C:\Users\aniva\AppData\Roaming\GeneralPlayer\GeneralPlayerData\GeneralPlayer.sqlite
            ContainerRepository containerRepo = null;
            
            //Build Folders
            Console.WriteLine("Creatig Directories.");
            ConfigurationFolders.FolderMaker.CreateFolders(true);
            Console.WriteLine("Directories Created.");

            //Copy Database
            Console.WriteLine("Coping Database.");
            if(File.Exists(DATABASENAME))
            {
                string target = ConfigurationFolders.BuildDirectoryPath(DATABASENAME, 
                        ConfigurationFolders.Folders.DatabaseFolder);
                containerRepo = new ContainerRepository(target);
                if (File.Exists(target))
                {
                    File.Delete(target);
                }

                File.Copy(DATABASENAME, 
                    ConfigurationFolders.BuildDirectoryPath(DATABASENAME, 
                        ConfigurationFolders.Folders.DatabaseFolder));
            }
            Console.WriteLine("Database Copied.");

            Container container = containerRepo.GetById(1);

            while (!Directory.Exists(container.Path))
            {
                Console.WriteLine("The Directory: {0} does not exists. Please specify a new Diectory", container.Path);
                container.Path = Console.ReadLine();
            }

            containerRepo.Update(container);

            Console.WriteLine("Building Content.");
            //BuildContent
            LocalFilesGateway gateway = new LocalFilesGateway(container.Path);
                var folders = gateway.GetContent() as List<System.IO.DirectoryInfo>;

                foreach (var folder in folders)
                {
                    var fileAdapter = new LocalFIleAdapter();
                    Console.WriteLine("Saving Collections");
                    Collection collection = fileAdapter.SaveDirectory(folder, 1);
                    Console.WriteLine("Collections Saved");
                    Console.WriteLine("Saving Files");
                    fileAdapter.SaveFilesInDirectory(folder, collection.Id);
                    Console.WriteLine("Files saved");
                }

            Console.WriteLine("Program Ready To Install");
            
        }
    }
}
