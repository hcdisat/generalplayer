﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using GeneralPlayer.Configuration;
using System.IO;
using System.Configuration;

namespace GeneralPlayerInstaller
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            String databaseName = ConfigurationManager.AppSettings["DB_NAME"];

            // Recreates All the folder
            ConfigurationFolders.FolderMaker.CreateFolders(true);
            ConfigurationFolders.FolderMaker.CreateDatabaseFolder(true);

            if (File.Exists(databaseName))
            {
                var target = ConfigurationFolders.BuildDirectoryPath(databaseName,
                        ConfigurationFolders.Folders.DatabaseFolder);

                if (File.Exists(target))
                {
                    File.Delete(target);
                }

                File.Copy(databaseName, target);                
            }

        }
    }
}
